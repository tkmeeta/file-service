package com.hj.util;

import com.hj.config.MinioConfig;
import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import io.minio.Result;
import io.minio.messages.Item;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dehui dou
 * @date 2020/10/12 14:27
 * @description
 */
@Component
public class MinioUtil {
    private static MinioClient minioClient = null;
    private static String BUCKETNAME;

    @Autowired
    private MinioConfig minioConfig;

    public void init() {
        String endpoint = minioConfig.getEndpoint();
        int port = minioConfig.getPort();
        String accessKey = minioConfig.getAccessKey();
        String secretKey = minioConfig.getSecretKey();
        boolean secure = minioConfig.getSecure();
        BUCKETNAME = minioConfig.getBucketName();
        try {
            minioClient = new MinioClient(endpoint, port, accessKey, secretKey, secure);
            if (!minioClient.bucketExists(BUCKETNAME)) {
                minioClient.makeBucket(BUCKETNAME);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 列出存储桶中的所有对象名称
     *
     * @param bucketName 存储桶名称
     * @return
     */
    public static List<String> listObjectNames(String bucketName) {
        List<String> listObjectNames = new ArrayList<>();
        try {
            Iterable<Result<Item>> myObjects = listObjects(bucketName);
            for (Result<Item> result : myObjects) {
                Item item = result.get();
                listObjectNames.add(item.objectName());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return listObjectNames;
    }

    /**
     * 列出存储桶中的所有对象
     *
     * @param bucketName 存储桶名称
     * @return
     */
    public static Iterable<Result<Item>> listObjects(String bucketName) {
        try {
            return minioClient.listObjects(bucketName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 文件上传
     *
     * @param multipartFile
     */
    public static void uploadFile(MultipartFile multipartFile, String filename, Map<String, String> params) {
        PutObjectOptions putObjectOptions = new PutObjectOptions(multipartFile.getSize(), PutObjectOptions.MIN_MULTIPART_SIZE);
        putObjectOptions.setContentType(multipartFile.getContentType());

        try {
            minioClient.putObject(BUCKETNAME, filename, multipartFile.getInputStream(), putObjectOptions);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 以字节数组的形式获取一个文件对象
     *
     * @param objectName 存储桶里的对象名称
     * @return
     */
    public static byte[] downloadFile(String objectName) {
        byte[] fileData = null;
        try {
            InputStream stream = minioClient.getObject(BUCKETNAME, objectName);
            fileData = IOUtils.toByteArray(stream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return fileData;
    }


    /**
     * 删除一个对象
     *
     * @param objectName 存储桶里的对象名称
     */
    public static void removeObject(String objectName) {
        try {
            minioClient.removeObject(BUCKETNAME, objectName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

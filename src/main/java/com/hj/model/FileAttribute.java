package com.hj.model;

/**
 * @author dehui dou
 * @date 2020/10/3 17:55
 * @description 文件相关参数,接口请求参数转换成对象
 */
public class FileAttribute {

    private FileType type;

    private String suffix;

    private String name;

    private String url;

    private String fileViewName;

    private String fileRealName;

    private String fileUniqueId;

    private String fileKey;

    public FileAttribute() {}

    public FileAttribute(FileType type, String suffix, String name, String url) {
        this.type = type;
        this.suffix = suffix;
        this.name = name;
        this.url = url;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileViewName() {
        return fileViewName;
    }

    public void setFileViewName(String fileViewName) {
        this.fileViewName = fileViewName;
    }

    public String getFileRealName() {
        return fileRealName;
    }

    public void setFileRealName(String fileRealName) {
        this.fileRealName = fileRealName;
    }

    public String getFileUniqueId() {
        return fileUniqueId;
    }

    public void setFileUniqueId(String fileUniqueId) {
        this.fileUniqueId = fileUniqueId;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    private FileAttribute(Builder builder) {
        this.type = builder.type;
        this.suffix = builder.suffix;
        this.name = builder.name;
        this.url = builder.url;
        this.fileViewName = builder.fileViewName;
        this.fileRealName = builder.fileRealName;
        this.fileUniqueId = builder.fileUniqueId;
        this.fileKey = builder.fileKey;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private FileType type;
        private String suffix;
        private String name;
        private String url;
        private String fileViewName;
        private String fileRealName;
        private String fileUniqueId;
        private String fileKey;

        public Builder type(FileType type) {
            this.type = type;
            return this;
        }

        public Builder suffix(String suffix) {
            this.suffix = suffix;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder fileViewName(String fileViewName) {
            this.fileViewName = fileViewName;
            return this;
        }

        public Builder fileRealName(String fileRealName) {
            this.fileRealName = fileRealName;
            return this;
        }

        public Builder fileUniqueId(String fileUniqueId) {
            this.fileUniqueId = fileUniqueId;
            return this;
        }

        public Builder fileKey(String fileKey) {
            this.fileKey = fileKey;
            return this;
        }

        public FileAttribute build() {
            return new FileAttribute(this);
        }
    }

}

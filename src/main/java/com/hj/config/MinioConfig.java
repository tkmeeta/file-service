package com.hj.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author dehui dou
 * @date 2020/10/12 14:26
 * @description
 */
@Component
public class MinioConfig {
    @Value("${minio.endpoint}")
    private String endpoint;
    @Value("${minio.port}")
    private int port;
    @Value("${minio.accessKey}")
    private String accessKey;
    @Value("${minio.secretKey}")
    private String secretKey;
    @Value("${minio.secure}")
    private Boolean secure;
    @Value("${minio.bucketName}")
    private String bucketName;

    public String getEndpoint() {
        return endpoint;
    }

    public int getPort() {
        return port;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public Boolean getSecure() {
        return secure;
    }

    public String getBucketName() {
        return bucketName;
    }
}
package com.hj.config;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class ThreadPoolConfig {
    @Value("${threadPool.taskExecutor.corePoolSize:10}")
    public Integer taskExecutorCorePoolSize;
    @Value("${threadPool.taskExecutor.maxPoolSize:20}")
    public Integer taskExecutorMaxPoolSize;
    @Value("${threadPool.taskExecutor.queueCapacity:50}")
    public Integer taskExecutorQueueCapacity;
    @Value("${threadPool.taskExecutor.keepAliveSeconds:300}")
    public Integer taskExecutorKeepAliveSeconds;
    @Value("${threadPool.taskExecutor.awaitTerminationSeconds:60}")
    public Integer taskExecutorAwaitTerminationSeconds;

    @Bean
    public Executor urlViewUpdateExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(taskExecutorCorePoolSize);
        // 设置最大线程数
        executor.setMaxPoolSize(taskExecutorMaxPoolSize);
        // 设置队列容量
        executor.setQueueCapacity(taskExecutorQueueCapacity);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(taskExecutorKeepAliveSeconds);
        // 设置默认线程名称
        executor.setThreadNamePrefix("taskExecutor-thread-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //关闭线程池时等待线程池任务, 最长等待时间
        executor.setAwaitTerminationSeconds(taskExecutorAwaitTerminationSeconds);
        return executor;
    }

    @Bean
    public Executor officeExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(taskExecutorCorePoolSize);
        // 设置最大线程数
        executor.setMaxPoolSize(taskExecutorMaxPoolSize);
        // 设置队列容量
        executor.setQueueCapacity(taskExecutorQueueCapacity);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(taskExecutorKeepAliveSeconds);
        // 设置默认线程名称
        executor.setThreadNamePrefix("taskExecutor-thread-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //关闭线程池时等待线程池任务, 最长等待时间
        executor.setAwaitTerminationSeconds(taskExecutorAwaitTerminationSeconds);
        return executor;
    }

    @Bean
    public Executor zipReaderExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(taskExecutorCorePoolSize);
        // 设置最大线程数
        executor.setMaxPoolSize(taskExecutorMaxPoolSize);
        // 设置队列容量
        executor.setQueueCapacity(taskExecutorQueueCapacity);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(taskExecutorKeepAliveSeconds);
        // 设置默认线程名称
        executor.setThreadNamePrefix("taskExecutor-thread-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //关闭线程池时等待线程池任务, 最长等待时间
        executor.setAwaitTerminationSeconds(taskExecutorAwaitTerminationSeconds);
        return executor;
    }
}
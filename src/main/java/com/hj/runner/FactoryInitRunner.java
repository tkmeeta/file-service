package com.hj.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.hj.service.FilePreviewFactory;
import com.hj.service.fileserver.FileServerFactory;

/**
 * @author dehui dou
 * @date 2020/10/3 17:58
 * @description 所有工厂类初始化Runner
 */
@Component
@Order(value = 1)
public class FactoryInitRunner implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
        FilePreviewFactory.init();
        FileServerFactory.init();
    }
}

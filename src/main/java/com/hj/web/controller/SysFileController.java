package com.hj.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hj.biz.SysFileBiz;
import com.hj.config.ConfigConstants;
import com.hj.model.FileAttribute;

/**
 * @author dehui dou
 * @date 2020/10/3 19:46
 * @description 文件服务Controller
 */
@Controller
@RequestMapping("/sys/file")
public class SysFileController {
    private static Logger logger = LoggerFactory.getLogger(SysFileController.class);

    @Autowired
    private SysFileBiz sysFileBiz;

    /**
     * @author dehui dou
     * @description 文件下载
     * @param file
     * @param systemCode
     * @param shareSystemCode
     * @param filePath
     * @return java.lang.String
     */
    @ResponseBody
    @RequestMapping(value = "/upload", method = {RequestMethod.POST, RequestMethod.GET})
    public String upload(@RequestParam("file") MultipartFile file,
                         @RequestParam(value = "systemCode",defaultValue = "ADMIN") String systemCode,
                         @RequestParam(value = "shareSystemCode", required = false) String shareSystemCode,
                         @RequestParam(value = "filePath", required = false) String filePath) {
        // 上传fastdfs 返回fileFdfsId；
        String fileUniqueId = sysFileBiz.upload(file, systemCode, shareSystemCode, filePath);
        return fileUniqueId;
    }

    /**
     * @author dehui dou
     * @description 文件下载
     * @param fileUniqueId
     * @param response
     * @return void
     */
    @ResponseBody
    @RequestMapping(value = "/download", method = {RequestMethod.POST, RequestMethod.GET})
    public void download(@RequestParam("fileUniqueId") String fileUniqueId,
                         HttpServletResponse response) {
        sysFileBiz.download(fileUniqueId, response);
    }

    /**
     * @author dehui dou
     * @description 图片查看
     * @param fileUniqueId
     * @param response
     * @return void
     */
    @ResponseBody
    @RequestMapping(value = "/image", method = {RequestMethod.POST, RequestMethod.GET})
    public void image(@RequestParam("fileUniqueId") String fileUniqueId,
                      HttpServletResponse response) {
        sysFileBiz.img(fileUniqueId, response);
    }

    /**
     * @author dehui dou
     * @description 文件在线预览 优先级高->低: url->压缩->普通预览
     * @param fileUniqueId
     * @param fileKey
     * @param officePreviewType
     * @param url
     * @param model
     * @return java.lang.String
     */
    @RequestMapping(value = "/view", method = {RequestMethod.POST, RequestMethod.GET})
    public String view(@RequestParam(value = "fileUniqueId", required = false) String fileUniqueId,
                       @RequestParam(value = "fileKey", required = false) String fileKey,
                       @RequestParam(value = "officePreviewType", required = false) String officePreviewType,
                       @RequestParam(value = "url", required = false) String url, Model model) {
        model.addAttribute("pdfDownloadDisable", ConfigConstants.getPdfDownloadDisable());
        model.addAttribute("officePreviewType", officePreviewType);
        FileAttribute fileAttribute =
                FileAttribute.builder().fileKey(fileKey).fileUniqueId(fileUniqueId).url(url).build();
        String view;
        try {
            view = sysFileBiz.view(model, fileAttribute);
        } catch (Exception e) {
            logger.error("系统错误", e);
            model.addAttribute("msg", "系统错误,请联系管理员");
            return "fileNotSupported";
        }
        return view;
    }
}

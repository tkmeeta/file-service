package com.hj.web.filter;

import java.io.IOException;

import javax.servlet.*;

import com.hj.config.WatermarkConfigConstants;

/**
 * @author dehui dou
 * @date 2020/10/3 19:49
 * @description 水印处理filter
 */
public class WatermarkConfigFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
        throws IOException, ServletException {
        String watermarkTxt = request.getParameter("watermarkTxt");
        request.setAttribute("watermarkTxt",
            watermarkTxt != null ? watermarkTxt : WatermarkConfigConstants.getWatermarkTxt());
        request.setAttribute("watermarkXSpace", WatermarkConfigConstants.getWatermarkXSpace());
        request.setAttribute("watermarkYSpace", WatermarkConfigConstants.getWatermarkYSpace());
        request.setAttribute("watermarkFont", WatermarkConfigConstants.getWatermarkFont());
        request.setAttribute("watermarkFontsize", WatermarkConfigConstants.getWatermarkFontsize());
        request.setAttribute("watermarkColor", WatermarkConfigConstants.getWatermarkColor());
        request.setAttribute("watermarkAlpha", WatermarkConfigConstants.getWatermarkAlpha());
        request.setAttribute("watermarkWidth", WatermarkConfigConstants.getWatermarkWidth());
        request.setAttribute("watermarkHeight", WatermarkConfigConstants.getWatermarkHeight());
        request.setAttribute("watermarkAngle", WatermarkConfigConstants.getWatermarkAngle());
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

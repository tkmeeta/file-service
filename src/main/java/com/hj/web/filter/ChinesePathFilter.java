package com.hj.web.filter;

import java.io.IOException;

import javax.servlet.*;

/**
 * @author dehui dou
 * @date 2020/10/3 19:48
 * @description 字符编码处理filter
 */
public class ChinesePathFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

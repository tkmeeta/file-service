package com.hj.biz.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hj.biz.dao.entity.SysRegisterSystem;

/**
 * @author dehui-dou
 * @date 2020/10/3 12:01
 * @description 系统注册mapper
 */
public interface SysRegisterSystemMapper extends BaseMapper<SysRegisterSystem> {

}

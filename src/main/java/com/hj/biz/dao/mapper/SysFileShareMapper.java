package com.hj.biz.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hj.biz.dao.entity.SysFileShare;

/**
 * @author dehui dou
 * @date 2020/10/2 14:33
 * @description 系统文件共享mapper
 */
public interface SysFileShareMapper extends BaseMapper<SysFileShare> {

}

package com.hj.biz.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hj.biz.dao.entity.SysRegisterSystem;
import com.hj.biz.dao.mapper.SysRegisterSystemMapper;
import com.hj.constants.CommonConstants.CommonStatusEnum;

/**
 * @author dehui dou
 * @date 2020/9/23 12:11
 * @description 文件系统注册dao
 */
@Component
public class SysRegisterSystemDao {

    @Autowired
    private SysRegisterSystemMapper sysRegisterSystemMapper;

    /**
     * @author dehui dou
     * @description 根据系统code和key 获取注册信息
     * @param systemCode
     * @param privateKey
     * @return com.hj.biz.dao.entity.SysRegisterSystem
     */
    public SysRegisterSystem getOneByCodeAndPrivateKey(String systemCode, String privateKey) {
        QueryWrapper<SysRegisterSystem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("system_code", systemCode);
        queryWrapper.eq("private_key", privateKey);
        queryWrapper.eq("disabled", CommonStatusEnum.ENABLED);
        List<SysRegisterSystem> fileList = sysRegisterSystemMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(fileList)) {
            return fileList.get(0);
        }
        return null;
    }
}

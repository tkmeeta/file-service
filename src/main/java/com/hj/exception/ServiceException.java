package com.hj.exception;

import org.springframework.http.HttpStatus;

/**
 * @author dehui dou
 * @date 2019/9/6 18:35
 * @description 业务层自定义异常
 */
public class ServiceException extends RuntimeException {
    private Integer errCode;
    private String errMsg;

    public ServiceException(Integer errCode, String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
        this.errCode = errCode;
    }

    public ServiceException(String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
        this.errCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public ServiceException() {
        this.errCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}

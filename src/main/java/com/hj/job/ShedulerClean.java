package com.hj.job;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hj.config.ConfigConstants;
import com.hj.util.FileUtils;

/**
 * @author dehui dou
 * @date 2020/10/3 17:49
 * @description 清理预览目录下所有文件
 */
@Component
public class ShedulerClean {

    private final Logger logger = LoggerFactory.getLogger(ShedulerClean.class);

    /**
     * @author dehui dou
     * @description 每晚3点执行
     */
    @Scheduled(cron = "0 0 3 * * ?")
    public void cleanViewData() {
        String fileDir = ConfigConstants.getFileDir();
        logger.info("Cache clean start");
        FileUtils.delAllViewFile(new File(fileDir));
        logger.info("Cache clean end");
    }
}

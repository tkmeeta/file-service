package com.hj.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.hj.model.FileAttribute;
import com.hj.service.FilePreview;
import com.hj.web.filter.BaseUrlFilter;

/**
 * @author dehui dou
 * @date 2020/10/3 19:13
 * @description 媒体类型处理实现类
 */
@Service
public class MediaFilePreviewImpl implements FilePreview {

    /**
     * @author dehui dou
     * @description 文件预览处理实现
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    @Override
    public String filePreviewHandle(Model model, FileAttribute fileAttribute) {
        String fileName = fileAttribute.getFileViewName();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        model.addAttribute("mediaUrl", BaseUrlFilter.getBaseUrl() + fileName);
        if ("flv".equalsIgnoreCase(extension)) {
            return "flv";
        }
        return "media";
    }

}

package com.hj.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.hj.model.FileAttribute;
import com.hj.service.FilePreview;

/**
 * @author dehui dou
 * @date 2020/10/3 18:42
 * @description cad文件处理类
 */
@Service
public class CadFilePreviewImpl implements FilePreview {

    /**
     * @author dehui dou
     * @description 文件预览处理实现
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    @Override
    public String filePreviewHandle(Model model, FileAttribute fileAttribute) {
        String fileName = fileAttribute.getFileViewName();
        String suffix = fileName.substring(fileName.indexOf(".") + 1);
        model.addAttribute("fileType", suffix);
        return "fileNotSupported";
    }
}

package com.hj.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.hj.model.FileType;
import com.hj.util.SpringUtils;

/**
 * @author dehui dou
 * @date 2020/10/3 19:19
 * @description 文件预览工厂类
 */
@Service
public class FilePreviewFactory {
    private static Map<String, FilePreview> filePreviewBeanMap = new ConcurrentHashMap();

    public static void init() {
        if (filePreviewBeanMap.size() == 0) {
            for (FileType e : FileType.values()) {
                String serviceBeanName = e.getInstanceName();
                filePreviewBeanMap.put(serviceBeanName, SpringUtils.getBean(serviceBeanName));
            }
        }
    }

    /**
     * @author dehui dou
     * @description 获取压缩内部文件预览实现类
     * @return com.hj.service.FilePreview
     */
    public static FilePreview getCompressInner() {
        Map<String, FilePreview> filePreviewMap = SpringUtils.getBeansOfType(FilePreview.class);
        return filePreviewMap.get("compressInnerFilePreviewImpl");
    }

    /**
     * @author dehui dou
     * @description 获取文件预览实现类
     * @return com.hj.service.FilePreview
     */
    public static FilePreview get(FileType fileType) {
        String instanceName = fileType.getInstanceName();
        try {
            if (StringUtils.isEmpty(instanceName)) {
                return null;
            }
            FilePreview filePreview = filePreviewBeanMap.get(instanceName);
            if (filePreview != null) {
                return filePreview;
            }
            filePreview = SpringUtils.getBean(instanceName);
            filePreviewBeanMap.put(instanceName, filePreview);
            return filePreview;
        } catch (Exception e) {
            return null;
        }
    }
}

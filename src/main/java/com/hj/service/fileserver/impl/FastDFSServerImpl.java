package com.hj.service.fileserver.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hj.service.fileserver.FileServer;
import com.hj.util.FastDFSClient;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dehui dou
 * @date 2020/10/3 18:00
 * @description fastdfs服务实现类
 */
@Service("fastDFSServerImpl")
public class FastDFSServerImpl implements FileServer {
    private static Logger logger = LoggerFactory.getLogger(FastDFSServerImpl.class);

    /**
     * @author dehui dou
     * @description 文件上传
     * @param file
     *            文件
     * @param fileExtName
     *            文件扩展名
     * @param params
     *            相关业务参数
     * @return java.lang.String
     */
    @Override
    public String upload(MultipartFile file, String fileExtName, Map<String, String> params) {
        Map<String, String> metaList = null;
        if (params != null) {
            metaList = new ConcurrentHashMap<>();
            metaList.put("systemCode", params.get("systemCode"));
            metaList.put("fileName", params.get("fileName"));
            metaList.put("filePath", params.get("filePath"));
            metaList.put("fileSize", params.get("fileSize"));
            metaList.put("createTm", params.get("createTm"));
        }
        String fileFdfsId = "";
        try {
            fileFdfsId = FastDFSClient.uploadFile(file.getBytes(), fileExtName, metaList);
        } catch (Exception e) {
            logger.error(String.format("文件上传失败"), e);
        }
        return fileFdfsId;
    }

    /**
     * @author dehui dou
     * @description 根据fastdfsId获取文件字节数据
     * @param fileId
     * @return byte[]
     */
    @Override
    public byte[] download(String fileId) {
        byte[] fileData = null;
        try {
            fileData = FastDFSClient.downloadFile(fileId);
        } catch (Exception e) {
            logger.error(String.format("文件下载失败 msg:%s", fileId), e);
        }
        return fileData;
    }

}

package com.hj.service.fileserver;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author dehui dou
 * @date 2020/10/3 18:07
 * @description 文件服务器通用接口
 */
public interface FileServer {
    String upload(MultipartFile file, String fileExtName, Map<String, String> params);

    byte[] download(String fileId);
}

package com.hj.service;

import org.springframework.ui.Model;

import com.hj.model.FileAttribute;

/**
 * @author dehui dou
 * @date 2020/10/3 19:16
 * @description 文件预览接口
 */
public interface FilePreview {

    /**
     * @author dehui dou
     * @description 文件预览方法
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    String filePreviewHandle(Model model, FileAttribute fileAttribute);
}

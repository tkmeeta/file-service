package org.artofsolving.jodconverter.office;

public enum OfficeConnectionProtocol {
    PIPE, SOCKET
}
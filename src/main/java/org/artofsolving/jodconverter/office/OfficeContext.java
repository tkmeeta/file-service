package org.artofsolving.jodconverter.office;

public interface OfficeContext {

    Object getService(String serviceName);

}
